# EJERCICIO 1: Contador secuencial

El programa recibe archivos como argumentos para realizar cálculos. Si no se ingresa ningún archivo, el programa no realiza nada. En caso contrario, para cada archivo ingresado se calcula la cantidad de líneas, palabras y caracteres. Luego, se entrega la cantidad total de líneas, palabras y caracteres, considerando todos los archivos ingresados. Para ejecutar esto, mediante un ciclo for, en la función principal, se recorren los archivos ingresados y, para cada uno, se realiza un llamado a tres funciones, las que abren y cierran el archivo en cada cálculo. Estas funciones corresponden a:

1. int contarLineas(char *nombre)
Utiliza la función getline() para recuperar las líneas del archivo a través de un ciclo while, por lo que en cada iteración se incrementa el contador de líneas.

2. int contarPalabras(char* nombre)
Se extraen las palabras del archivo en un ciclo while y se incrementa el contador de palabras en cada iteración.

3. int contarCaracteres(char *nombre)
Se extraen los caracteres utilizando la función get() y se incrementa el contador de caracteres en cada iteración.

Cada función retorna el contador correspondiente y este, en el ciclo for de la función principal, recibe los resultados y los acumula, para finalmente entregar los resultados totales.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:

* g++ secuencial.cpp -o secuencial

Luego, para ejecutar el programa se debe utilizar:

* ./secuencial [archivos]

Para este caso, se ha trabajado con dos archivos: transhumanismo.txt y vitis.txt
Ejemplo 1 de ejecución:
* ./secuencial transhumanismo.txt vitis.txt
* Tiempo: 0.000947 segundos

También se puede trabajar con archivos más grandes: ciborg.pdf
Ejemplo 2 de ejecución:
* ./secuencial ciborg.pdf
* Tiempo: 0.010268 segundos

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/soyr/-/tree/main/lab3/ej1

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Enlaces consultados
* https://www.cplusplus.com/reference/istream/istream/get/
