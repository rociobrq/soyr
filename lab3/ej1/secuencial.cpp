#include <iostream>
#include <fstream>
using namespace std;

int contarLineas(char *nombre){
    ifstream archivo;
    string linea;
    int numLineas = 0;

    archivo.open(nombre);

    /* Se obtienen líneas hasta leer completamente el archivo */
    while(getline(archivo, linea)){
        numLineas += 1;
    }

    archivo.close();

    /* Resultado número de líneas del archivo */
    cout << "N° de líneas: " << numLineas << endl;
    return numLineas;
}

int contarPalabras(char* nombre){
    ifstream archivo;
    string palabra;
    int numPalabras = 0;

    archivo.open(nombre);

    /* Se extraen las palabras del archivo */
    while(archivo >> palabra){
        numPalabras += 1;
    }

    archivo.close();

    /* Resultado número de palabras del archivo */
    cout << "N° de palabras: " << numPalabras << endl;
    return numPalabras;

}

int contarCaracteres(char *nombre){
    ifstream archivo;
    char caracter;
    int numCaracteres = 0;

    archivo.open(nombre);

    /* Se extraen los caracteres del archivo */
    while(archivo.get(caracter)){
        numCaracteres += 1;
    }

    archivo.close();

    /* Resultado final del número de caracteres del archivo */
    cout << "N° de caracteres: " << numCaracteres << endl;
    return numCaracteres;

}

float calcularTiempo(float tiempoInicial, float tiempoFinal){
    /* Calcula el tiempo de ejecución del programa */
    float tTotal = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;

    return tTotal;
}

int main(int argc, char *argv[]){
    /* Verifica que se hayan ingresado archivos */
    if(argc <= 1){
        cout << "Debe ingresar archivos de texto..." << endl;
    }

    int totalLineas = 0;
    int totalPalabras = 0;
    int totalCaracteres = 0;
    float tiempoInicial;
    float tiempoFinal;

    tiempoInicial = clock();

    /* Cálculos por cada archivo ingresado */
    for(int i = 1; i < argc; i++){
        cout << "Nombre del archivo: " << argv[i] << endl;
        /* Se pasa el nombre del archivo como argumento para arbrirlo a partir
        del nombre en cada función */
        /* Recepción de cantidades por cada archivo */
        totalLineas += contarLineas(argv[i]);
        totalPalabras += contarPalabras(argv[i]);
        totalCaracteres += contarCaracteres(argv[i]);
        cout << "\n" << endl;
    }

    tiempoFinal = clock();

    /* Se imprimen resultados totales */
    cout << "Total de líneas: " << totalLineas << endl;
    cout << "Total de palabras: " << totalPalabras << endl;
    cout << "Total de caracteres: " << totalCaracteres << endl;
    /* Se imprime tiempo de ejecución */
    cout << "Ejecución: " << calcularTiempo(tiempoInicial, tiempoFinal) << endl;

    return 0;
}
