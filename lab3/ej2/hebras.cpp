#include <iostream>
#include <fstream>
#include <pthread.h>
using namespace std;

/* Estructura para recepcionar archivos y guardar variables */
struct estructura{
    string nombre;
    ifstream archivo;
    int numLineas = 0;
    int numPalabras = 0;
    int numCaracter = 0;
};

/* Declaración de la estructura para cada archivo */
estructura contenido;

void *contarLineas(void *nombre){
    /* Recepción del nombre */
    char* nombre_char = (char*)nombre;
    string linea;
    int numLineas = 0;

    /* Se abre archivo y se actualiza nombre */
    contenido.nombre = nombre_char;
    contenido.archivo.open(nombre_char);

    /* Se obtienen líneas hasta leer completamente el archivo */
    while(getline(contenido.archivo, linea)){
        numLineas += 1;
    }
    contenido.archivo.close();

    /* Resultado número de líneas del archivo */
    /* Se actualiza número de líneas en la estructura */
    contenido.numLineas = numLineas;
    printf("N° de líneas de %s: %d\n", contenido.nombre.c_str(), numLineas);
    pthread_exit(0);
}

void *contarPalabras(void *nombre){
    /* Recepción del nombre */
    char* nombre_char = (char*)nombre;
    string palabra;
    int numPalabras = 0;

    /* Se abre el archivo y se actualiza nombre */
    contenido.nombre = nombre_char;
    contenido.archivo.open(nombre_char);

    /* Se extraen las palabras del archivo */
    while(contenido.archivo >> palabra){
        numPalabras += 1;
    }

    contenido.archivo.close();

    /* Resultado número de palabras del archivo */
    /* Se actualiza número de palabras en la estructura */
    contenido.numPalabras = numPalabras;
    printf("N° de palabras de %s: %d\n", contenido.nombre.c_str(), numPalabras);
    pthread_exit(0);
}

void *contarCaracteres(void *nombre){
    /* Recepción del nombre */
    char* nombre_char = (char*)nombre;
    int numCaracter = 0;
    char caracter;

    /* Se abre el archivo y se actualiza nombre */
    contenido.nombre = nombre_char;
    contenido.archivo.open(nombre_char);

    /* Se extraen los caracteres del archivo */
    while(contenido.archivo.get(caracter)){
        numCaracter += 1;
    }

    contenido.archivo.close();

    /* Resultado final del número de caracteres del archivo */
    /* Se actualiza número de caracteres en la estructura */
    contenido.numCaracter = numCaracter;
    printf("N° de caracteres de %s: %d\n", contenido.nombre.c_str(), numCaracter);
    pthread_exit(0);

}

float calcularTiempo(float tiempoInicial, float tiempoFinal){
    /* Calcula el tiempo de ejecución del programa */
    float tTotal = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;

    return tTotal;
}

int main(int argc, char *argv[]){
    /* Verifica que se hayan ingresado archivos */
    if(argc <= 1){
        cout << "Debe ingresar archivos de texto..." << endl;
    }

    pthread_t hebras[argc-1];
    int totalCaracteres = 0;
    int totalPalabras= 0;
    int totalLineas = 0;
    float tiempoInicial;
    float tiempoFinal;

    tiempoInicial = clock();

    /* Crea los hebras */
    for(int i = 1; i < argc; i++){
        cout << "Nombre del archivo: " << argv[i] << endl;
        pthread_create(&hebras[i-1], NULL, contarLineas, argv[i]);
        pthread_join(hebras[i-1], NULL);
        pthread_create(&hebras[i-1], NULL, contarPalabras, argv[i]);
        pthread_join(hebras[i-1], NULL);
        pthread_create(&hebras[i-1], NULL, contarCaracteres, argv[i]);
        pthread_join(hebras[i-1], NULL);

        /* Recepción de cantidades por cada archivo */
        totalLineas += contenido.numLineas;
        totalPalabras += contenido.numPalabras;
        totalCaracteres += contenido.numCaracter;
    }

    tiempoFinal = clock();

    /* Se imprimen resultados totales */
    cout << "Total de líneas: " << totalLineas << endl;
    cout << "Total de palabras: " << totalPalabras << endl;
    cout << "Total de caracteres: " << totalCaracteres << endl;
    /* Se imprime tiempo de ejecución */
    cout << "Ejecución: " << calcularTiempo(tiempoInicial, tiempoFinal) << endl;
    return 0;

}
