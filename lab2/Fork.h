#ifndef FORK_H
#define FORK_H

#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
using namespace std;

class Fork {
    private:
        pid_t pid;
        string video;

    public:
        /* Constructor de la clase */
        Fork();
        Fork (string enlace);

        /* Métodos para crear procesos y ejecutar la descarga y reproducción */
        void crear_fork();
        void programa();
};
#endif
