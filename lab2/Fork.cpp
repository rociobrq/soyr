
#include <iostream>
using namespace std;
/* Se incorpora definición de clase */
#include "Fork.h"

Fork::Fork(){}

Fork::Fork(string enlace) {
    /* Se asigna enlace del video a variable */
    this->video = enlace;

    /* Creación del proceso y ejecución del programa para descargar mp3 */
    crear_fork();
    programa();
}

/* Métodos */
void Fork::crear_fork() {
    /* Fork proceso hijo */
    pid = fork();
}

void Fork::programa() {

    /* Valida la creación de proceso hijo */
    if (pid < 0) {
        cout << "Se ha producido un error..." << endl;

    } else if (pid == 0) {
        /* Código del proceso hijo */
        cout << "Descargando video y extrayendo audio..." << endl;
        cout << "Enlace de descarga: " << video << endl;

        /* Comando para realizar la extacción del audio en formato mp3. Archivo
        mantiene nombre original y se le añade la extensión mp3 */
        execlp("youtube-dl", "youtube-dl", "-x", "--audio-format", "mp3", "-o",
            "%(title)s.%(ext)s", video.c_str(), NULL);

    } else {
        /* Código del proceso padre */
        /* Padre espera por el término del proceso hijo */
        wait(NULL);
        string mp3;

        /* Se filtran archivos de acuerdo a última fecha de modificación, luego
        con grep se filtran los mp3 y con head -1 se obtiene el primer archivo,
        se añaden comillas y con xargs se pasa el nombre como argumento al
        reproductor mpg123 */
        mp3 = R"raw(ls -c|grep mp3|head -1|awk '{v = "\047"; print v $0 v}'|
            xargs mpg123)raw";

        /* Se utiliza el intérprete con el parámetro -c para leer el comando
        de la variable mp3 y ejecutarlo */
        execlp("/bin/sh","sh", "-c", mp3.c_str(), NULL);
    }
}
