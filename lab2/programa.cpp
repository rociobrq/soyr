/* $ g++ programa.cpp Fork.cpp -o programa */

#include <iostream>
using namespace std;
/* Clase donde se creará el fork y se realizará descarga-reproducción del mp3*/
#include "Fork.h"

int main (int argc, char *argv[]) {
    string enlace;

    /* Valida ingreso de 2 argumentos */
    if(argc != 2){
        cout << "Se ha producido un error.";
    }
    else{
        /* Recepción de enlace de video */
        enlace = argv[1];

        /* Creación proceso */
        Fork procesoVideo(enlace);
    }

    return 0;
}
