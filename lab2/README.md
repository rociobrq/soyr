# REPRODUCTOR MP3

El programa permite extraer el audio en formato mp3 de un video y reproducirlo. Para esto, al ejecutar el programa se debe ingresar el enlace correspondiente a un video de YouTube y, a partir de esto, se creará un proceso hijo que se encargará de descargar y extraer el audio en el formato correspondiente. Luego, el proceso padre realizará una búsqueda en el directorio y reproducirá el último archivo en formato mp3. A raíz de esto, si en el proceso hijo no se logra descargar el audio, se reproducirá el último audio descargado, siempre que existan otros mp3.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++
* mpg123
* YouTube-dl

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

3. Para instalar el reproductor de audio, ejecutar:
* sudo apt-get install mpg123

4. Para instalar youtube-dl, ejecutar:
* sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
* sudo chmod a+rx /usr/local/bin/youtube-dl

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:

* g++ programa.cpp Fork.cpp -o programa

Luego, para ejecutar el programa se debe utilizar:

* ./programa [enlace del video]

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.
* mpg123: Reproductor de audio.
* YouTube-dl: Programa para descargar vídeos o extraer audio de sitios de streaming.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0
* mpg123 1.25.13
* YouTube-dl 2021.06.06

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/soyr/-/tree/main/lab2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Enlaces consultados
* https://superuser.com/questions/276596/play-mp3-or-wav-file-via-the-linux-command-line
* https://www.cplusplus.com/reference/string/string/c_str/
* https://github.com/ytdl-org/youtube-dl/blob/master/README.md#output-template
